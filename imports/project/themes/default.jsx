// @flow

import { createMuiTheme } from "@material-ui/core/styles";
import { lightTheme } from 'shakeapp-web-app-ui/imports/project/themes/light';

export const defaultTheme = createMuiTheme({
  ...lightTheme,
});
