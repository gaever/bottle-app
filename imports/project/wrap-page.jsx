// @flow

import React from 'react';

// import { AnaliticsProvider } from './packages/analitics';
import { wrapSsrGql } from '../outer/hasura/ssr';

import 'normalize.css';
import { ThemeProvider } from '@material-ui/styles';
import { defaultTheme } from './themes/default';

import 'shakeapp-web-app-ui/imports/project/wrap-page';

/**
 * Base app page wrapper. Provide ssr gql and analitics.
 * @param {function} Component
 * @returns {function} WrappedComponent
 */

export const wrapPage = (Component: any) => {
  return wrapSsrGql({
    // gqlSecret: process.env.GQL_SECRET,
    Component: () => {
      return (
        <ThemeProvider theme={defaultTheme}>
          <Component />
        </ThemeProvider>
      );
    },
  });
};
