import Knex from 'knex';

export function up(knex: Knex) {
  return knex.schema.createTable('chats', (table) => {
    table.increments('id').primary();
    table.timestamps(false, true);

    table.timestamp('deleted_at', { useTz: true });
    table.timestamp('opened_at', { useTz: true });

    /**
     * Chat could or could not have assigned operator_id.
     * Chat suppose to be in the waiting queue
     * if there is no operator assigned.
     */
    table
      .integer('user_id')
      .unsigned()
      .references('user.id')
      .onUpdate('CASCADE')
      .onDelete('SET NULL');

    table
      .integer('with_user_id')
      .unsigned()
      .references('user.id');

    table
      .integer('unread_messages')
      .unsigned()
      .notNullable()
      .defaultTo(0);
  });
}

export function down(knex: Knex) {
  return knex.schema.dropTable('chats');
}
