import Knex from 'knex';

export function up(knex: Knex) {
  return knex.schema.createTable('messages', (table) => {
    table.increments('id').primary();
    table.timestamps(false, true);

    table.timestamp('deleted_at', { useTz: true });

    table
      .text('message')
      .notNullable()
      .defaultTo('');

    table
      .integer('user_id')
      .unsigned()
      .references('user.id')
      .onUpdate('CASCADE')
      .onDelete('SET NULL');

    table.boolean('out');
    table.boolean('viewed');
  });
}

export function down(knex: Knex) {
  return knex.schema.dropTable('messages');
}
