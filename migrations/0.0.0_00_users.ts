import Knex from 'knex';

export function up(knex: Knex) {
  return knex.schema.createTable('users', (table) => {
    table.increments('id').primary();
    table.timestamps(false, true);
    table.timestamp('deleted_at', { useTz: true });

    table
      .specificType('login', 'varchar(64)')
      .notNullable()
      .unique();

    table
      .specificType('name', 'varchar(64)')
      .notNullable()
      .defaultTo('Operator');
  });
}

export function down(knex: Knex) {
  return knex.schema.dropTable('users');
}
