// @flow

import React, { useState } from 'react';

import 'normalize.css';
import { wrapPage } from '../imports/project/wrap-page';
import { Container } from '@material-ui/core';

import { Nav } from './index';

export default wrapPage(() => {
  return <>
    <Nav value="forms"/>
    <Container>
    </Container>
  </>;
});
