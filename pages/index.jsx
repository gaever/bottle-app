// @flow

import _ from 'lodash';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import moment from 'moment';

import Link from 'next/link';

import 'normalize.css';
import { useAuth } from '../imports/deepcase/auth/react';
import { wrapPage } from '../imports/project/wrap-page';
import { Button, Paper, Tabs, Tab } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useGql } from '../imports/deepcase/hasura/use';

import { AbsoluteCenter } from 'shakeapp-web-app-ui/imports/project/components/absolute-center';
import { SelectRange } from 'shakeapp-web-app-ui/imports/project/components/time/select-range';
import { Drawer, HandlerArrow } from 'shakeapp-web-app-ui/imports/project/components/drawer';
import DisplayGeolocation from '../imports/project/sandbox/DisplayGeolocation';
import TakePicture from '../imports/project/native-modules/TakePicture';

export const Nav = ({ value }: { value: string }) => {
  const router = useRouter();

  return <Paper square style={{
    position: 'sticky', top: 0, left: 0,
    zIndex: 2,
    width: '100%',
    backgroundColor: '#fff'
  }}>
    <Tabs
      value={value}
      indicatorColor="primary"
      textColor="primary"
      variant="fullWidth"
      centered
    >
      <Tab value="index" label="Index" onClick={() => router.push("/")}/>
      <Tab value="list" label="List" onClick={() => router.push("/list")}/>
      <Tab value="forms" label="Forms" onClick={() => router.push("/forms")}/>
    </Tabs>
  </Paper>;
};

const disabledDates = [moment().add(-2, 'month').toDate()];
for (let i = 0; i < 30; i++) {
  disabledDates.push(moment(disabledDates[i]).add(_.random(5, 10), 'days').toDate());
}

export default wrapPage(() => {
  return <>
  </>;
});
