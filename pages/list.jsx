// @flow

import _ from 'lodash';
import React, { useState } from 'react';

import Link from 'next/link';

import 'normalize.css';
import { useAuth } from '../imports/deepcase/auth/react';
import { wrapPage } from '../imports/project/wrap-page';
import { Button, Paper, Tabs, Tab, List, ListItem, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useGql } from '../imports/deepcase/hasura/use';

import { AbsoluteCenter } from 'shakeapp-web-app-ui/imports/project/components/absolute-center';

import { Nav } from './index';

const list = _.times(100, (i) => (
  <ListItem key={i} button>
    <ListItemText primary={i}/>
  </ListItem>
));

export default wrapPage(() => {
  const [open, setOpen] = useState(true);
  const [value, setValue] = useState(0);

  const toggleDrawer = (open) => event => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) return;
    setOpen(open);
  };

  return <>
    <Nav value="list"/>
    <List>
      {list}
    </List>
  </>;
});
