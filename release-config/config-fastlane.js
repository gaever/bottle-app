const fs = require('fs');

// destination.txt will be created or overwritten by default.
fs.copyFile('./ios/Fastfile', '../ios/App/fastlane/Fastfile', (err) => {
  if (err) throw err;
  console.log('iOS Fastfile configured');
});

fs.copyFile('./android/Fastfile', '../android/fastlane/Fastfile', (err) => {
  if (err) throw err;
  console.log('Android Fastfile configured');
});